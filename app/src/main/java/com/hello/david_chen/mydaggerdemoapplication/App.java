package com.hello.david_chen.mydaggerdemoapplication;

import android.content.SharedPreferences;

import com.hello.david_chen.mydaggerdemoapplication.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {
    @Inject SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }
}
