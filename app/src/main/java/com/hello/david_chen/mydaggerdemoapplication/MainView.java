package com.hello.david_chen.mydaggerdemoapplication;

public interface MainView {
    void onPhotoFetched(String title, String url);
}
