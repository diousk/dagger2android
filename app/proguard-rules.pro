# Retrofit 2.X
## https://square.github.io/retrofit/ ##
-dontwarn okio.**
-dontwarn javax.annotation.**
-dontwarn retrofit2.**
-dontwarn org.codehaus.mojo.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions
-keepattributes *Annotation*

-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations

-keepattributes EnclosingMethod

-keepclasseswithmembers class * {
    @retrofit2.* <methods>;
}

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keepclasseswithmembers interface * {
    @retrofit2.* <methods>;
}

# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

# retrofit 1
-dontwarn com.squareup.okhttp.**
-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }
-keep class org.apache.http.** { *; }
-keep class org.apache.james.mime4j.** { *; }
-keep class javax.inject.** { *; }
-keep class retrofit.** { *; }

-keep class me.soocii.socius.totalsolution.serverapi.request.** { *; }
-keep class me.soocii.socius.staging.totalsolution.serverapi.request.** { *; }
-keep class me.soocii.socius.debug.totalsolution.serverapi.request.** { *; }

-keep class me.soocii.socius.totalsolution.serverapi.response.** { *; }
-keep class me.soocii.socius.staging.totalsolution.serverapi.response.** { *; }
-keep class me.soocii.socius.debug.totalsolution.serverapi.response.** { *; }


# To enable ProGuard in your project, edit project.properties
# to define the proguard.config property as described in that file.
#
# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in ${sdk.dir}/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the ProGuard
# include property in project.properties.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepclasseswithmembers class * extends android.content.pm.IPackageStatsObserver {
    public <methods>;
}

-keep interface android.content.pm.** { *; }

-keepclassmembers class *{
    @android.webkit.JavascriptInterface  *;
}

-keep public class com.android.vending.billing.* {
    public protected *;
}

-keep public class android.content.pm.* {
    public protected *;
}

#Google Analytics
-keep class com.google.analytics.** { *; }
-keep class com.google.android.gms.** { *; }
-keep class com.google.tagmanager.** { *; }

# Google API client - URL shortener
-keep class com.google.api.services.urlshortener.** { *; }

# event bus
-keepclassmembers,includedescriptorclasses class ** {
    public void onEvent*(**);
}

# GameKingSDK
-ignorewarnings

-keep class android.support.v4.** { *; }
-keep class com.facebook.** { *; }
-keep class com.handmark.** { *; }
-keep class com.fedorvlasov.lazylist.** { *; }
-keep class com.flurry.** { *; }
-keep class com.google.analytics.** { *; }

-keepattributes JavascriptInterface
-keepattributes Signature

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class * { //replace by gmobi
#    @android.webkit.JavascriptInterface <methods>;
#}


# fix for dependent libs
-dontwarn org.xmlpull.**
-keep class org.xmlpull.v1.**
{
	*;
}

#Google Play Server
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

# Keep SafeParcelable value, needed for reflection. This is required to support backwards
# compatibility of some classes.
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

# Keep the names of classes/members we need for client functionality.
-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

# Needed for Parcelable/SafeParcelable Creators to not get stripped
-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

# Fix view pager bug
-keepclassmembers class android.support.v4.view.ViewPager { private boolean mFirstLayout; }

# keep libstreaming classes
-keep class me.soocii.rtmp.** { *; }

-keep class com.googlecode.javacv.**{ *; }
-keepclassmembers class com.googlecode.javacv.** {
    <methods>;
}
-keep class com.googlecode.javacpp.**{ *; }
-keepclassmembers class com.googlecode.javacpp.** {
    <methods>;
}

# keep custem banner
-keep class me.soocii.socius.core.customview.carousel.** { *; }

# fabric crashlytics
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**

-keep,allowobfuscation @interface android.support.annotation.Keep
-keep @android.support.annotation.Keep class *

-keepclassmembers class * {
    @android.support.annotation.Keep *;
}

# dagger
-dontwarn dagger.android.**
